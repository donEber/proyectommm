import Vue from 'vue'
import Router from 'vue-router'
import Inicio from './views/Inicio.vue'
import Ministro from './views/Ministro.vue'
import Noticia from './views/Noticia'
import Objetivos from './views/Objetivos'
Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'inicio',
      component: Inicio
    },
    {
      path: '/ministro',
      name: 'ministro',
      component: Ministro
    },
    {
      path: '/noticia',
      name: 'noticia',
      component: Noticia
    },
    {
      path: '/objetivos',
      name: 'objetivos',
      component: Objetivos
    },
  ]
})
